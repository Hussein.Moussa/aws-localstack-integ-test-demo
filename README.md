# AWS local stack testing

In this example I am using [Local Stack](https://github.com/localstack/localstack) to 
setup a local AWS environment to be able to run integration test.

The code is doing the following

* Download a file from S3 bucket.
* Transform the content of the downloaded file (upper case the content).
* Upload the transformed customer profile to S3 bucket.

## How to run

### Prepare the environment
To run environment you can use the script `./start.sh` this will do the following
* Start LocalStack container.
* Create the S3 bucket that will be used in the test.
* Copy the files from local `test_data` directory to S3 where the test code expects.

### Run integration test

To run the integration test, you could use `python test_customer-profile-transformer.py`


## Clean  up

To cleanup your local environment you could use `./stop.sh`

## References

* [How to fake AWS locally with LocalStack](https://dev.to/goodidea/how-to-fake-aws-locally-with-localstack-27me)
* [Python, Boto3, and AWS S3: Demystified](https://realpython.com/python-boto3-aws-s3/)