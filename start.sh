#!/usr/bin/env bash

#docker-compose -f docker-compose.yml -f spark-docker-compose.yml up -d
docker-compose -f docker-compose.yml up -d

aws --endpoint-url=http://localhost:4572 s3 mb s3://demo-bucket
aws --endpoint-url=http://localhost:4572 s3api put-bucket-acl --bucket demo-bucket --acl public-read

# Upload the test
aws --endpoint-url=http://localhost:4572 s3 cp ./test_data/ s3://demo-bucket --recursive