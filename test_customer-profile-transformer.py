from unittest import TestCase
import unittest
from au.com.samples.nbd import *

S3_FILE = 'testFile.txt'


class CustomerProfileTransformerTest(TestCase):
    target = CustomerProfileTransformer()

    def test_transformed_dir_got_cleaned_up(self):
        self.target.transform_customer_profile(S3_FILE)
        transformed_dir_path = f'{CustomerProfileTransformer.CUSTOMER_PROFILE_PROCESSING_DIR}'
        self.assertListEqual(os.listdir(transformed_dir_path), [])

    def test_upload_transformed_customer_profile(self):
        transformed_s3_file_name = self.target.transform_customer_profile(S3_FILE)
        expected_file = open(f'./test_data/{S3_FILE}', 'r')
        expected_customer_profile = expected_file.read().upper()
        local_transformed_profile_path = self.target._download_customer_profile(transformed_s3_file_name)
        transformed_profile_file = open(f'{CustomerProfileTransformer.CUSTOMER_PROFILE_PROCESSING_DIR}/{local_transformed_profile_path}', 'r')

        self.assertEqual(expected_customer_profile, transformed_profile_file.read())
        expected_file.close()
        transformed_profile_file.close()


if __name__ == '__main__':
    unittest.main()
