import os
from pathlib import Path

import boto3


def _get_resource_for(service_name):
    aws_endpoint_url = 'http://localhost:4572'
    return boto3.resource(service_name, endpoint_url=aws_endpoint_url)


class CustomerProfileTransformer:
    CUSTOMER_PROFILE_PROCESSING_DIR = '/tmp/in_progress_transformed_profiles'
    BUCKET_NAME = 'demo-bucket'

    def _store_transformed_customer_profile(self, file_name, customer_profile):
        file = open(f'{self.CUSTOMER_PROFILE_PROCESSING_DIR}/{file_name}', 'w')
        file.write(customer_profile)

    def _upload_transformed_customer_profile(self, file_name):
        full_customer_profile_file_path = f'{self.CUSTOMER_PROFILE_PROCESSING_DIR}/{file_name}'
        s3_resource = _get_resource_for('s3')
        s3_resource.Object(bucket_name=self.BUCKET_NAME, key=file_name).upload_file(full_customer_profile_file_path)
        return full_customer_profile_file_path

    def transform_customer_profile(self, s3_file):
        Path(self.CUSTOMER_PROFILE_PROCESSING_DIR).mkdir(parents=True, exist_ok=True)
        customer_transformed_file_name = 'transformed_with_uuid.txt'
        local_customer_profile_file_name = self._download_customer_profile(s3_file)
        transformed_customer_profile = self._do_transform_profile(local_customer_profile_file_name)

        self._store_transformed_customer_profile(customer_transformed_file_name, transformed_customer_profile)
        self._upload_transformed_customer_profile(customer_transformed_file_name)
        self._cleanup_in_progress_files([local_customer_profile_file_name, customer_transformed_file_name])
        return customer_transformed_file_name

    def _do_transform_profile(self, local_customer_profile_file_name):
        full_customer_profile_file_path = f'{self.CUSTOMER_PROFILE_PROCESSING_DIR}/{local_customer_profile_file_name}'
        customer_profile = open(full_customer_profile_file_path, 'r').read()
        transformed_customer_profile = customer_profile.upper()
        return transformed_customer_profile

    def _download_customer_profile(self, s3_file):
        customer_profile_source = _get_resource_for('s3')
        path, filename = os.path.split(s3_file)
        downloaded_file_path = f'{self.CUSTOMER_PROFILE_PROCESSING_DIR}/{filename}'
        customer_profile_s3_object = customer_profile_source.Object(bucket_name=self.BUCKET_NAME, key=s3_file)
        customer_profile_s3_object.download_file(downloaded_file_path)
        return filename

    def _cleanup_in_progress_files(self, local_files_list):
        for f in local_files_list:
            os.remove(f'{self.CUSTOMER_PROFILE_PROCESSING_DIR}/{f}')
